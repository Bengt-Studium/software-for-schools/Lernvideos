# Kenntnisse

Unsere Kenntnisse von Programmiersprachen und zugehörigen Werkzeugen, Bibliotheken und Frameworks.

Progammiersprache | Kenntnis Bengt | Kenntnis Dennis | Summe
--- | --- | --- | ---
C | 2 | 8 | 10
Java | 16 | 24 | 40
C++ | 0 | 12 | 12
Objective-C | 0 | 0 | 0
C# | 0 | 32 | 32
PHP | 8 | 16 | 24
(Visual) Basic | 2 | 8 | 10
Python | 32 | 2 | 34
Perl | 0 | 1 | 1
Ruby | 1 | 2 | 3
Javascript | 16 | 24 | 40
Visual Basic .NET | 0 | 24 | 24
Lisp | 2 | 4 | 6
Pascal | 0 | 12 | 12
Delphi / Objecitve Pascal | 0 |  12 | 12
BASH | 16 | 4 | 20
Assembly | 2 | 2 | 4
Lua | 0 | 8 | 8

## Legende

Zahl | Programmiersprachenkenntnis 
---- | ---
32 | Lieblingsprogrammiersprache, auch weniger wichtige Werkzeuge, Bibliotheken und Frameworks bekannt
16 | In größeren Projekten benutzt, wichtige Werkzeuge, Bibliotheken und Frameworks bekannt
8 | mehrfach benutzt, einige Werkzeuge, Bibliotheken und Frameworks bekannt
4 | in Uni-Modul kennengelernt, kaum Werkzeuge, Bibliotheken und Frameworks bekannt
2 | Grundkenntnisse, Sprache im Gröbsten erlernt
1 | Hello World, nur grober Eindruck vorhanden
0 | nie benutzt

# Lerninteresse

Unser Interesse die jeweilige Programmiersprache durch Einsatz in diesem Projekt besser oder überhaupt kennen zu lernen.

Progammiersprache | Lerninteresse Bengt | Lerninteresse Dennis | Summe
--- | --- | --- | ---
C | 1 | -1 | 0
Java | -1 | 0 | -1
C++ | 0 | -1 | -1
Objective-C | -1 | -1  | -2
C# | -1 | 0 | -1
PHP | -1 | -1 | -2
(Visual) Basic | -1 | -1 | -2
Python | 2 | 1 | 3
Perl | -1 | 0 | -1
Ruby | 1 | 2 | 3
Javascript | 1 | 2 | 3
Visual Basic .NET | -1 | -1 | -2
Lisp | -1 | -1 |-2
Pascal | -1 | -1 | -2
Delphi / Objecitve Pascal | -1 | -1 | -2
BASH | -1 | -1 | -2
Assembly | -1 | -1 | -2
Lua | -1 | 2 | 1

## Legende

Zahl | Programmiersprachenkenntnis 
---- | ---
4 | sehr großes Interesse
2 | großes Intereesse
1 | geringes Interesse
0 | kein Interesse
-1 | kategorische Ablehnung
