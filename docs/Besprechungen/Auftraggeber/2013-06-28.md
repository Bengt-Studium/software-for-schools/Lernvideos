##  2.1 Anwendungsfälle

-   Übersichliches Use-Case Diagramm wird gewünscht.
-   Abhängigkeiten müssen zwischen Anwendungsfällen überprüft werden.
-   Abhängigkeiten sollen in Inhaltsverzeichnis deutlich werden

##  2.3 Produktfunktion

Import aus SMB-Freigabe ist Muss-Anforderung.

##  2.4 Domänenmodell

-   Einleitung soll auf drei folgende Abschnitte hinweisen.
    -   Videodokumente sind das, was eingeordnet wird.
    -   Einordnung in Hiearachie ist typisches Vorgehen.
    -   Einordnung in Parallelhierarchie ist möglich.

###  2.4.2 Hierarchie

-   Erklärung überarbeiten
    -   Klassenstufe hier sowas wie "Schuljahrgang" meint.
    -   mit Grafik konsistent halten
-   Formulierungsvorschläge:
    -   "Klassenstufe bildet Ordnung bezüglich des Jahrgangs"
    -   "Klassenstufe umfasst alle Klassen eines Jahrgangs"
    -   "auf einander aufbauende Definitionen geben"
-   Korrektur: Videos können für ein oder mehrere [Themen vorgesehen werden]
-   Korrektur: Hierarchie ergibt sich aus Curricula

##  2.5 Nutzermerkmale

-   erste beiden Punkte in Einleitung der Überschrift umformen
-   den letzten Punkt aufteilen
-   Merkmale beschreiben, nicht Anforderungen stellen

###  2.5.2 Lehrer

-   Lehrer tauschen sich eher nicht aus.
-   Lehrer brauchen gezielte Anweisung zum Benutzen des Systems.
-   Lehrer nutzt schriftlich in Papierform festgehaltene Richtlinien.
-   Lehrer kann Dateien in Verzeichnis ablegen.

###  2.5.3 Admins

-   Admin hat die Fähigkeit sich mit voller Komplexität des Systems.
-   Admin hat durch viel Erfahrung mit Verwaltungs- und Management-Software "Empathie" für solche Systeme aufgebaut.

##  2.7 Abhängigkeiten

-   Tablets sind ausreichend zum Einsatz im Unterricht vorhanden
-   Tablets haben moderne Browser, beherrschen Großteil von HTML5

##  3.3 Leistungsanforderungen

-   Vorschlag für Test-Szenario: 5 Geräte streamen Videos über ein WLAN mit 54 MBit/s

### 3.3.2

-   ist Muss-Anforderung

#   5

-   Idee: Benutzerhandbücher für Lehrer und Schüler?
