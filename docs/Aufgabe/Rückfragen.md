Aufgabenstellung laut Stud.IP

>   Die Aufgabe besteht darin ein bestehendes System, welches automatisch
>   Webseiten und einen Index für Videos aus einer Dateiserver-
>   Verzeichnisstruktur erstellt zu reimplementieren und ggf. zu erweitern.
>   Das System soll mit HTML5/Ruby(ggf. Ruby on Rails) implementiert werden.
>   
>   Teilaufgaben, die in diesem Zusammenhang zu Lösen sind sind z.B.:
>   
>   -   parsen von Textdateien, um die relevanten Informationen zu entnehmen.
>   -   automatisches Umcodieren und Optimieren von Videodateien
>   -   Integration des Systems in ein bestehes Schulnetz
>   -   ggf. Entwicklung eins Webinterfaces zum Upload
>   -   Implementierung eines Players mit Kapitelmarken
>   -   ...

Nachfragen

-   Was soll das System umfassen?
    -   Video-Konverter?
        -   buy?
    -   Webinterface?
        -   make?
    -   Player?
    -   -   buy?
-   Warum reimplementieren?
    -   "Weil Prototyp mangelhaft ist."?
        -   Soll/Darf der Prototyp evaluiert werden?
-   Warum Index?
    -   "Ist trivial zu machen."?
    -   "Es gibt nur wenige Videos."?
-   Warum HTML5?
    -   "alte Browser können vernachlässigt werden."
    -   "Video-Element kann genutzt werden."
-   Warum Ruby/Rails?
    -   "Was der Admin auch weiterpflegen kann."
        -   Sind evtl. andere Programmiersprachen erlaubt? (Python?)
