# Lernvideos

Ein System zum Sammeln und Verbreiten von Videos im Schulumfeld.

## Repository aufsetzen

Dieses Repository benutzt Git Submodule, um unsere modifizierte Version von Mediacore Community Edition und Reveal.js einzubinden. Diese Abhängigikeiten richtet der folgende Befehl ein:

    $ git submodule update --init --recursive

## System aufsetzen

Siehe Abschnitt "Administratorhandbuch" im "Anhang" der Datei `docs/Anforderungsdokument/Anforderungsdokument.pdf`.
