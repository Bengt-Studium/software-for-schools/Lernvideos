#! /bin/python

from __future__ import print_function
from __future__ import with_statement
from __future__ import division

import time
import os
import json

import requests

# configuration
SMB_ROOT = "."                                  # relative or absolute SMB path
URL = "http://localhost:8080/upload/submit"     # upload submit endpoint URL
DELAY = 1                                       # Time between checks in seconds

# Run indefinitely
while True:

    # interate through the whole directory tree beneath SMB_ROOT
    for root, dirs, files in os.walk(SMB_ROOT):

        # handle files, only
        for filename in files:

            # Try to read JSON data from te current file
            file_path = os.path.join(root, filename)
            try:
                with open(file_path, "r") as json_data:
                    payload = json.load(json_data)
            except ValueError as error:
                print("Warn: Skipping file '%s' due to: %s" % (file_path, \
                                                               error))
                continue
                
            # make a request using the gatered data
            r = requests.post(URL, payload)
            
            # delete file
            os.remove(file_path)
    
    # wait a certain time before running again
    time.sleep(DELAY)

