from gevent import monkey; monkey.patch_all()

from time import sleep
from bottle import route, run, template

#@route('/stream')
#def stream():
#    yield 'START'
#    sleep(3)
#    yield 'MIDDLE'
#    sleep(5)
#    yield 'END'

@route('/hello/:name')
def index(name='World'):
    return template('<b>Hello {{name}}</b>!', name=name)

run(host='localhost', port=8080, server='gevent')

